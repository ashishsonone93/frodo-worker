'use strict';
var app = angular.module('app', []);
app.controller('AppController', ['$scope', '$location', '$interval', function($scope, $location, $interval){
  $scope.BASE_URL = $location.protocol() + "://" + $location.host() + ":" + $location.port() + '/worker'; // http://192.168.0.25:9999/worker";
  
  $scope.inputDivVisible = true;
  $scope.processDivVisible = false;
  $scope.outputDivVisible = false;

  var checkStatus;

  $scope.toast = function(msg, isError){
    $scope.message = msg;
    $scope.messageError = isError || false;
  };
  
  $scope.reset = function(){
    $scope.fileName = "";
    
    $scope.message = "";
    $scope.messageError = false;

    $scope.jobId = "";
    $scope.jobStatus = "";
    $scope.jobResult = "";
    $scope.jobProgress = "";
    
  };
  
  $scope.init = function(){
    $scope.reset();
  };
  
  $scope.filePicked = function(event){
    $scope.reset();
    
    var files = event.target.files;
    console.log('filePicked haha %j', files[0]);
    var prefix = new Date().getTime() - new Date('2017-01-01').getTime();
    var name = files[0].name;
    
    name = name.replace(/[^a-zA-Z0-9._-]+/g, '');
    
    $scope.fileName = prefix + '-' + name;
    console.log('filePicked fileName ', $scope.fileName);
    $scope.$apply();
  };
  
  $scope.upload = function(){
    $scope.toast("upload starting");
  }
  

$scope.restart = function(){
      $scope.toast("starting new search");
      $scope.inputDivVisible = true;
      $scope.processDivVisible = false;
      $scope.outputDivVisible = false;
  }


  $scope.send = function(){
    console.log("send called");
    if ($scope.fileName.length == 0){
      $scope.toast("Please pick a file", true);
      return;
    }

    $scope.inputDivVisible =false;
    $scope.processDivVisible = true;
    $scope.outputDivVisible = false;

    
    var url = $scope.BASE_URL + '/csv-jobs/'
    var data = {
      fileName : $scope.fileName
    };
    
    $.ajax({
      url : url,
      type : "POST",
      data : data,
      success: function(data, textStatus, xhr) {
        $scope.jobId = data.id;
        $scope.jobStatus = "submitted";
        $scope.jobProgress = "0%";
        checkStatus = $interval( function() { $scope.status(data.id)}, 2000);
        $scope.toast("submitted the job for background crunching");
        $scope.$apply();
      },
      error: function(xhr, textStatus, err) {
        $scope.toast("failed to submit job. please retry");
        $scope.$apply();
      }
    });
  }
  
  $scope.status = function(jobId){
    console.log("status called");
    if (jobId.length == 0){
      $scope.toast("please submit the job first", true);
      return;
    }
    
    var url = $scope.BASE_URL + '/csv-jobs/' + jobId
    $.ajax({
      url : url,
      type : "GET",
      success: function(data, textStatus, xhr) {
        var job = data.job;
        $scope.jobStatus = job.state;
        $scope.jobProgress = job.progress + "%";

        if(angular.isDefined(job.result && job.result.result)){
          if (angular.isDefined(checkStatus)) {
            $interval.cancel(checkStatus);
            checkStatus = undefined;

              $scope.inputDivVisible = false;
              $scope.processDivVisible = false;
              $scope.outputDivVisible = true;
          }
        }

        $scope.jobResult = job.result && job.result.result;
        $scope.$apply();
      },
      error: function(xhr, textStatus, err) {
        $scope.toast("failed to check status of job " + jobId);
        $scope.$apply();
      }
    });
  }
  
}]);

app.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
  /*
  <input type="file" custom-on-change="uploadFile">

  app.controller('myCtrl', function($scope){
    $scope.uploadFile = function(event){
      var files = event.target.files;
    };
  });  
  */
});
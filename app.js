var express = require('express')
var Hashids = require('hashids');
var intHasher = new Hashids('frodo');

var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended : true})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); //parse application/json

/*
export REDIS_URL='redis://localhost:6379'
export PORT=8012

# REDIS_URL='redis://localhost:6379' PORT=8012 node app.js
*/

var KUE_PREFIX = process.env.KUE_PREFIX || 'q'; //prefix for jobs stored by kue
var REDIS_URL = process.env.REDIS_URL || 'redis://localhost:6379';

var PORT = process.env.PORT || 8011;

//= = = = = = = KUE queue = = = = = = = = = =
var kue = require('kue');
var queue = kue.createQueue({
  redis: REDIS_URL
}); //default options
queue.watchStuckJobs(6000); //Unstable Redis connections

queue.on('job enqueue', function(id, type){
  console.log( 'Job %s got queued of type %s', id, type );
}).on('job complete', function(id, result){
  console.log('completed job %j %j', id, result);
}).on('job failed', function(id, errMsg){
  console.log('failed job %j %j', id, errMsg);
});

queue.on( 'error', function( err ) {
  console.log( 'Oops... ', err );
});

//app.use('/kue-ui', kue.app);

// = = = = routes setup = = = = =
var router = express.Router();

router.get('/csv-jobs/:id', function(req, res){
  var id = intHasher.decode(req.params.id)[0] || -1;
  
  kue.Job.get(id, function(err, job){
    res.json({
      err : err,
      job : job
    });
  });
});

router.post('/csv-jobs/', function(req, res){
  console.log("POST /csv-jobs/ %j", req.body);
  if(!req.body.fileName){
    res.status(400);
    return res.json({
      success : false,
      err: 'required [fileName]'
    });
  }
  var fileName = req.body.fileName;
  var job = queue.create('csv', {
      task : 'process_email_csv', //implies will use process_email_csv.py to process this job
      title : 'csv analysis of ' + fileName,
      fileName : fileName
  }).save(function(err){
    console.log('job save : err %j | id %j', err, job.id);
    
    res.json({
      success : !err,
      id : intHasher.encode(job.id)
    });
    
  });
});

//= = = = = = = = = = = = = = = = = = =
//var path = require('path');
// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');

// index.js continued
router.use('/js', express.static(__dirname + "/web/js"));
router.use('/web', express.static(__dirname + "/web/html"));
router.use('/images', express.static(__dirname + "/web/images"));
router.use('/css', express.static(__dirname + "/web/css"));

/*
app.post('/', upload.single('avatar'), function (req, res, next) {
  // req.file is the `avatar` file
  // req.body will hold the text fields, if there were any
  console.log(req.body); //form fields
  console.log(req.file); //form files
  
  res.json({success : true, file : req.file, body : req.body});
});
*/

//internal health check
app.get('/worker/health', function(req, res){
  res.json({
    success : true
  });
});

app.use('/worker/', router);
app.listen(PORT);
console.log("Server listening on localhost:" + PORT);
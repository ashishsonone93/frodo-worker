import sys
import time
import os

srcFilePath = sys.argv[1]
outFilePath = sys.argv[2]
SCRIPTS_DIR = os.path.dirname(os.path.realpath(__file__))
CURR_DIR = os.path.abspath('.')

print('copy started');
time.sleep( 5 )
f = open(srcFilePath)
fo = open(outFilePath, 'w')
fo.write('CURR_DIR=' + CURR_DIR + '\n')
fo.write('SCRIPTS_DIR=' + SCRIPTS_DIR + '\n')
for l in f:
  fo.write(l)

f.close()
fo.close()
print('copy complete');

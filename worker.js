//= = = = google cloud storage = = = = 
var gcs = require('@google-cloud/storage')({
  projectId: 'api-project-271983337950',
  keyFilename: './json/API-Project-12062907ddc8.json'
});

// Reference an existing bucket. 

var GCS_BUCKET = 'frodo-test';
var GCS_BUCKET_URL = 'https://storage.googleapis.com/frodo-test/';
var bucket = gcs.bucket(GCS_BUCKET);

var DOWNLOAD_FOLDER = __dirname + '/downloads/';
var UPLOAD_FOLDER = __dirname + '/uploads/';

//= = = = = = = = = = = = = = = = = = =
var PythonShell = require('python-shell');

/*
export REDIS_URL='redis://localhost:6379'
export SCRIPTS_FOLDER='/home/ashish/scripts'
export PYTHON_PATH='/home/ashish/anaconda3/bin/python'

# REDIS_URL='redis://localhost:6379' SCRIPTS_FOLDER='/home/ashish/scripts' PYTHON_PATH='/home/ashish/anaconda3/bin/python' node worker.js
*/

var KUE_PREFIX = process.env.KUE_PREFIX || 'q'; //prefix for jobs stored by kue
var REDIS_URL = process.env.REDIS_URL || 'redis://localhost:6379';

var SCRIPTS_FOLDER = process.env.SCRIPTS_FOLDER || (__dirname + '/scripts/');
var PYTHON_PATH = process.env.PYTHON_PATH || '/home/ashish/anaconda3/bin/python';

/*
var script = 'analyze.py'

app.get('/exec', function(req, res){
  console.log(script);
  var options = {
    pythonPath: PYTHON_PATH,
    scriptPath: SCRIPTS_FOLDER,
    mode: 'text',
    args: ['datafile.csv', 'false']
  };
  
  PythonShell.run(script, options, function (err, results) {
    console.log('finished err %j', err);
    if (err) {
      res.json({
        success : false,
        err : err
      });
    }
    else{
      console.log('results: %j', results);
      res.json({
        success : true,
        results : results
      });
    }
  });
  
});

app.get('/exec-2', function(req, res){
  console.log(script);
  var options = {
    pythonPath: '/home/ashish/anaconda3/bin/python',
    scriptPath: SCRIPTS_FOLDER,
    mode: 'text',
    args: ['datafile.csv', 'false']
  };
  
  var shell = new PythonShell(script, options);
  shell.on('message', function(message){
    console.log('message -> %j', message)
  });
  
  // end the input stream and allow the process to exit 
  shell.end(function (err) {
    console.log('finished %j', err);
    res.json({success : (!err), err : err});
  });
  
});
*/

//= = = = = = = KUE = = = = = = = = = =
var kue = require('kue');
var queue = kue.createQueue({
  redis: REDIS_URL
}); //default options
queue.watchStuckJobs(6000); //Unstable Redis connections

queue.on('job enqueue', function(id, type){
  console.log( 'Job %s got queued of type %s', id, type );
}).on('job complete', function(id, result){
  console.log('completed job %j %j', id, result);
}).on('job failed', function(id, errMsg){
  console.log('failed job %j %j', id, errMsg);
});

queue.on( 'error', function( err ) {
  console.log( 'Oops... ', err );
});

/*
queue.process('data', 2, function(job, done){
  console.log('[worker] starting job %j', job.id);
  job.progress(90, 100, 'halfway there ' + job.id);
  setTimeout(function(){
    job.progress(80, 100, 'complete ' + job.id);

    if(job && job.data && job.data.file && job.data.file.length > 10){
      console.log('[worker] failed job ' + job.id);
      done(new Error('too long name'));
    }
    else{
      console.log('[worker] completed ' + job.id);
      done(null, {output : 'out-' + job.data.file});
    }
  }, 10000);
  
});

*/

//= = = = = = = WORKER = = = = = = = = = =
function work(job, done){
  console.log('job picked for processing %j', job);
  job.progress(10, 100, 'started csv download ');
  
  var srcFileName = job.data.fileName;
  var task = job.data.task;
  
  var srcFilePath = DOWNLOAD_FOLDER + srcFileName;
  
  var outFileName = 'out-' + srcFileName;
  var outFilePath = UPLOAD_FOLDER + outFileName;
  
  console.log('downloading %j', srcFileName);
  var p = bucket.file(srcFileName).download({
    destination: srcFilePath
  });
  
  p = p.then(function(result){
    job.progress(20, 100, 'download csv over. Now processing.');
    console.log('download over. Running script %j.py from folder %j', task, SCRIPTS_FOLDER);
    
    //work on the file using python script
    var options = {
      pythonPath: PYTHON_PATH,
      scriptPath: SCRIPTS_FOLDER,
      mode: 'text',
      args: [srcFilePath, outFilePath]
    };

    var script = task + '.py'; //copy.py
    
    PythonShell.run(script, options, function (err, results) {
      job.progress(90, 100, 'script processing over. Now uploading');
      if (err) {
        console.log('error executing analysis script');
        done(new Error('error executing analysis script'));
      }
      else{
        console.log('analysis over');
        // Upload a local file to a new file to be created in your bucket. 
        bucket.upload(outFilePath, function(err, file) {
          if (!err) {
            // "zebra.jpg" is now in your bucket.
            console.log('upload success : task over');
            job.progress(100, 100, 'Upload success');
            done(null, {result : GCS_BUCKET_URL + outFileName});
          }
          else{
            console.log('upload failed');
            done(new Error('error uploading file'));
          }
        });
      }
    });
  });
  
  p.catch(function(err){
    console.log('download failed ' + srcFileName);
    done(new Error('download failed ' + srcFileName));
  });
}

queue.process('csv', 1, work);

//= = = = = = = = = = = = = = = = = = =
console.log("Worker waiting for jobs");